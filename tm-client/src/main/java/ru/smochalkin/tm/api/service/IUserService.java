package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User findByLogin(@Nullable String login);

    void removeByLogin(@Nullable String login);

    @NotNull
    User create(@NotNull String login, @NotNull String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    void setPassword(@Nullable String userId, @Nullable String password);

    void updateById(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    boolean isLogin(@Nullable String login);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
