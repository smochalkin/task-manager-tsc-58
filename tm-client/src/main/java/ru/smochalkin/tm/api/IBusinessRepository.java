package ru.smochalkin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    void clear(String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @Nullable
    E findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    E findByIndex(@NotNull String userId, int index);

    void removeByName(@NotNull String userId, @NotNull String name);

    void removeByIndex(@NotNull String userId, @NotNull Integer index);

    void updateById(@NotNull String id, @NotNull String name, @Nullable String desc);

    void updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @Nullable String desc);

    void updateStatusById(@NotNull String id, @NotNull Status status);

    void updateStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

    void updateStatusByIndex(@NotNull String userId, int index, @NotNull Status status);

    int getCountByUser(@NotNull String userId);

}
