package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.api.IBusinessService;
import ru.smochalkin.tm.model.Task;

public interface ITaskService extends IBusinessService<Task> {
}
