package ru.smochalkin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractProjectListener;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectUpdateStatusByIdListener extends AbstractProjectListener {

    @Override
    @NotNull
    public String name() {
        return "project-status-update-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Update project status by id.";
    }

    @Override
    @EventListener(condition = "@projectUpdateStatusByIdListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusName = TerminalUtil.nextLine();
        @NotNull final Result result = projectEndpoint.changeProjectStatusById(sessionService.getSession(), id, statusName);
        printResult(result);
    }

}
