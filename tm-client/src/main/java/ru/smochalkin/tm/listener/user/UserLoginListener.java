package ru.smochalkin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.endpoint.SessionDto;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public class UserLoginListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "login";
    }

    @Override
    @NotNull
    public String description() {
        return "Log in.";
    }

    @Override
    @EventListener(condition = "@userLoginListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final SessionDto session = sessionEndpoint.openSession(login, password);
        sessionService.setSession(session);
    }

}
