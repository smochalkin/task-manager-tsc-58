package ru.smochalkin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.endpoint.TaskDto;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskShowListByProjectIdListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "task-show-by-project-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Show tasks by project id.";
    }

    @Override
    @EventListener(condition = "@taskShowListByProjectIdListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter project id: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final List<TaskDto> tasks = taskEndpoint.findTasksByProjectId(sessionService.getSession(), projectId);
        for (TaskDto task : tasks) {
            System.out.println(task.getId() + "|" + task.getName() + "|" + task.getStatus());
        }
    }

}
