package ru.smochalkin.tm.exception.system;

import ru.smochalkin.tm.exception.AbstractException;

public class UserIsLocked extends AbstractException {

    public UserIsLocked() {
        super("Error! User is locked...");
    }

}
