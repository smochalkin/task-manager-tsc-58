package ru.smochalkin.tm.exception.system;

import ru.smochalkin.tm.exception.AbstractException;

public final class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! Command not found...");
    }

}