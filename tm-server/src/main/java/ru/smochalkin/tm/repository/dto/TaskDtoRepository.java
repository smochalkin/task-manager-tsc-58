package ru.smochalkin.tm.repository.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.smochalkin.tm.api.repository.dto.ITaskDtoRepository;
import ru.smochalkin.tm.dto.TaskDto;

import javax.persistence.EntityManager;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
public class TaskDtoRepository extends AbstractDtoRepository<TaskDto> implements ITaskDtoRepository {

    public void clear() {
        entityManager
                .createQuery("DELETE FROM TaskDto e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM TaskDto e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @NotNull
    public TaskDto findById(@Nullable final String id) {
        return entityManager.find(TaskDto.class, id);
    }

    @Override
    public TaskDto findByName(@Nullable final String userId, @Nullable final String name) {
        return entityManager
                .createQuery(
                        "SELECT e FROM TaskDto e WHERE e.name = :name AND e.userId = :userId",
                        TaskDto.class
                )
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @NotNull
    public List<TaskDto> findAll() {
        return entityManager.createQuery("SELECT e FROM TaskDto e", TaskDto.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAllByUserId(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT e FROM TaskDto e WHERE e.userId = :userId", TaskDto.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDto findByIdAndUserId(@Nullable final String userId, @Nullable final String id) {
        return entityManager
                .createQuery(
                        "SELECT e FROM TaskDto e WHERE e.id = :id AND e.userId = :userId", TaskDto.class
                )
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public TaskDto findByIndex(@Nullable final String userId, @NotNull final Integer index) {
        return entityManager
                .createQuery("SELECT e FROM TaskDto e WHERE e.userId = :userId", TaskDto.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    public void remove(@NotNull final TaskDto entity) {
        TaskDto reference = entityManager.getReference(TaskDto.class, entity.getId());
        entityManager.remove(reference);
    }

    public void removeById(@Nullable final String id) {
        TaskDto reference = entityManager.getReference(TaskDto.class, id);
        entityManager.remove(reference);
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM TaskDto e WHERE e.name = :name AND e.userId = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIdAndUserId(@Nullable final String userId, @NotNull final String id) {
        entityManager
                .createQuery("DELETE FROM TaskDto e WHERE e.userId = :userId AND e.id=:id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM TaskDto e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    @Override
    public int getCount() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM TaskDto e", Long.class)
                .getSingleResult().intValue();
    }

    @Override
    public int getCountByUser(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM TaskDto e WHERE e.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

    @Override
    public void bindTaskById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        entityManager
                .createQuery(
                        "UPDATE TaskDto e SET e.projectId = :projectId WHERE e.userId = :userId AND e.id = :id"
                )
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void unbindTaskById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        entityManager
                .createQuery(
                        "UPDATE TaskDto e SET e.projectId = NULL WHERE e.userId = :userId AND e.projectId = :projectId AND e.id = :id"
                )
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<TaskDto> findTasksByUserIdAndProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        return entityManager
                .createQuery(
                        "SELECT e FROM TaskDto e WHERE e.userId = :userId AND e.projectId = :projectId",
                        TaskDto.class
                )
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeTasksByProjectId(@Nullable final String projectId) {
        entityManager
                .createQuery(
                        "DELETE FROM TaskDto e WHERE e.projectId = :projectId"
                )
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}