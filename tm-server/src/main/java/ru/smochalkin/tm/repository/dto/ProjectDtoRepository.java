package ru.smochalkin.tm.repository.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.smochalkin.tm.api.repository.dto.IProjectDtoRepository;
import ru.smochalkin.tm.dto.ProjectDto;

import javax.persistence.EntityManager;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
public class ProjectDtoRepository extends AbstractDtoRepository<ProjectDto> implements IProjectDtoRepository {

    public void clear() {
        entityManager
                .createQuery("DELETE FROM ProjectDto e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectDto e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    public @NotNull ProjectDto findById(@Nullable final String id) {
        return entityManager.find(ProjectDto.class, id);
    }

    @Override
    public ProjectDto findByName(@Nullable final String userId, @Nullable final String name) {
        return entityManager
                .createQuery(
                        "SELECT e FROM ProjectDto e WHERE e.name = :name AND e.userId = :userId",
                        ProjectDto.class
                )
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @NotNull
    public List<ProjectDto> findAll() {
        return entityManager
                .createQuery("SELECT e FROM ProjectDto e", ProjectDto.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAllByUserId(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT e FROM ProjectDto e WHERE e.userId = :userId", ProjectDto.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDto findByIdAndUserId(@Nullable final String userId, @Nullable final String id) {
        return entityManager
                .createQuery(
                        "SELECT e FROM ProjectDto e WHERE e.id = :id AND e.userId = :userId", ProjectDto.class
                )
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public ProjectDto findByIndex(@Nullable final String userId, @NotNull final Integer index) {
        return entityManager
                .createQuery("SELECT e FROM ProjectDto e WHERE e.userId = :userId", ProjectDto.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    public void remove(@NotNull final ProjectDto entity) {
        ProjectDto reference = entityManager.getReference(ProjectDto.class, entity.getId());
        entityManager.remove(reference);
    }

    public void removeById(@Nullable final String id) {
        ProjectDto reference = entityManager.getReference(ProjectDto.class, id);
        entityManager.remove(reference);
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM ProjectDto e WHERE e.name = :name AND e.userId = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIdAndUserId(@Nullable final String userId, @NotNull final String id) {
        entityManager
                .createQuery("DELETE FROM ProjectDto e WHERE e.userId = :userId AND e.id=:id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM ProjectDto e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    @Override
    public int getCount() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM ProjectDto e", Long.class)
                .getSingleResult()
                .intValue();
    }

    @Override
    public int getCountByUser(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM ProjectDto e WHERE e.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

}