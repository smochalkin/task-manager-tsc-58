package ru.smochalkin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.listener.JpaEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
@EntityListeners(JpaEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class SessionDto extends AbstractEntityDto implements Cloneable {

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @Nullable
    private String signature;

    @Column(name = "time_stamp")
    private long timestamp = System.currentTimeMillis();

    public SessionDto(@NotNull final String userId) {
        this.userId = userId;
    }

    @Override
    public SessionDto clone() {
        try {
            return (SessionDto) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
