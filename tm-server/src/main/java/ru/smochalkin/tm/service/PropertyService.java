package ru.smochalkin.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.smochalkin.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Value("#{environment['iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['secret']}")
    private String passwordSecret;

    @Value("#{environment['sign.iteration']}")
    private Integer signIteration;

    @Value("#{environment['sign.secret']}")
    private String signSecret;

    @Value("#{environment['server.host']}")
    private String serverHost;

    @Value("#{environment['server.port']}")
    private String serverPort;

    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @Value("#{environment['database.username']}")
    private String databaseUser;

    @Value("#{environment['database.password']}")
    private String databasePassword;

    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @Value("#{environment['database.sql_dialect']}")
    private String databaseDialect;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2ddlAuto;

    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @Value("#{environment['cache.level']}")
    private String useSecondLevelCache;

    @Value("#{environment['cache.query']}")
    private String useQueryCache;

    @Value("#{environment['cache.minimal-puts']}")
    private String useMinimalPuts;

    @Value("#{environment['cache.prefix']}")
    private String cacheRegionPrefix;

    @Value("#{environment['cache.config']}")
    private String cacheProviderConfig;

    @Value("#{environment['cache.factory']}")
    private String cacheRegionFactory;

}

