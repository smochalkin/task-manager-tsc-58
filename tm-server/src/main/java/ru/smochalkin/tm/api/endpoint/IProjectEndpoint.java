package ru.smochalkin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.ProjectDto;
import ru.smochalkin.tm.dto.SessionDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {
    @WebMethod
    @SneakyThrows
    Result createProject(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    );

    @WebMethod
    @SneakyThrows
    Result changeProjectStatusById(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "id") @NotNull String id,
            @WebParam(name = "status") @NotNull String status
    );

    @WebMethod
    @SneakyThrows
    Result changeProjectStatusByIndex(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "index") @NotNull Integer index,
            @WebParam(name = "status") @NotNull String status
    );

    @WebMethod
    @SneakyThrows
    Result changeProjectStatusByName(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "status") @NotNull String status
    );

    @WebMethod
    @SneakyThrows
    List<ProjectDto> findProjectAllSorted(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "sort") @NotNull String strSort
    );

    @WebMethod
    @SneakyThrows
    List<ProjectDto> findProjectAll(
            @WebParam(name = "session") @NotNull SessionDto sessionDto
    );

    @WebMethod
    @SneakyThrows
    ProjectDto findProjectById(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "id") @NotNull String id
    );

    @WebMethod
    @SneakyThrows
    ProjectDto findProjectByName(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "name") @NotNull String name
    );

    @WebMethod
    @SneakyThrows
    ProjectDto findProjectByIndex(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "index") @NotNull Integer index
    );

    @WebMethod
    @SneakyThrows
    Result removeProjectById(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "id") @NotNull String id
    );

    @WebMethod
    @SneakyThrows
    Result removeProjectByName(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "name") @NotNull String name
    );

    @WebMethod
    @SneakyThrows
    Result removeProjectByIndex(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "index") @NotNull Integer index
    );

    @WebMethod
    @SneakyThrows
    Result clearProjects(
            @WebParam(name = "session") @NotNull SessionDto sessionDto
    );

    @WebMethod
    @SneakyThrows
    Result updateProjectById(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "id") @NotNull String id,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    );

    @WebMethod
    @SneakyThrows
    Result updateProjectByIndex(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "index") @NotNull Integer index,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    );
}
